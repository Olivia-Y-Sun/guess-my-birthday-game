
# Instead of replying "yes" and "no", reply with "y" for "yes", "l" for "later", and "e" for "earlier".
# Have the computer take into account your response and adjust its range of guessing each time based on your reply.
# Make it so the computer prints the month name instead of the number. For example, have it prompt with the message
# "Guess 1: Syed, were you born in January 1996?"
# Allow user to choose how many times does he/she want to try.

from datetime import date, timedelta
from random import randint

# Use the algorithm of binary search to divid the searching range by halve, if early than bday, looking for in the
# left half, if later, in the right half. When the max meet min, bingo!

def finder(response, high_date_d, low_date_d, mid_date_d, guess_flag):

    if response == "y":
        guess_flag = True       # User types "y", job done!
        print("I Knew it!")
        pass
    elif response == "e":       # left half
        high_date_d = mid_date_d - timedelta(days=1)
        if high_date_d <= low_date_d:
            high_date_d = low_date_d
            guess_flag = True
    elif response == "l":       # right half
        low_date_d = mid_date_d + timedelta(days=1)
        if low_date_d >= high_date_d:
            low_date_d = high_date_d
            guess_flag = True
    else:
        pass

    delta = high_date_d - low_date_d
    delta = delta.days
    mid_date_d = low_date_d + timedelta(days=delta // 2)

    return low_date_d, high_date_d, mid_date_d, guess_flag

low_date_d = date(1924, 1, 1)
high_date_d = date(2021, 12, 31)
delta = high_date_d - low_date_d
delta = delta.days
mid_date_d = low_date_d + timedelta(days=delta // 2)

my_name = input("What is your name? ")
num_guesses = int(input("How many guesses does the computer get?"))

guess_flag = False

for num_guess in range(num_guesses):
    if guess_flag == False:
        # showing the date according to the format of "YYYY-mon-DD"
        print("Guess ", num_guess + 1, my_name, "were you born in ", mid_date_d.strftime('%Y-%b-%d'))
        response = input("yes or earlier or later? You can type 'y' for yes, 'e' for earlier and 'l' for later: ")
        low_date_d, high_date_d, mid_date_d, guess_flag = finder(response, high_date_d, low_date_d, mid_date_d, guess_flag)
    else:
        print("I got it! It has to be ", mid_date_d.strftime('%Y-%b-%d'), "!")
        break

if guess_flag == False:
    print("I have other things to do, bye.")

